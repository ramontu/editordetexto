//TREBALL REALITZAT PER RAMON TRILLA
//APLICAT 1.1 AVANÇAT TODO ACABAR
//####################################################################################################
//#					BUGS	TODO						     #
//####################################################################################################
//
//
//
//
//####################################################################################################
#include <stdio.h>
const int C_Column = 15;  //NO TOCAR POR NADA DEL MUNDO MUNDIAL
const int C_Files = 5;
const int C_Windows = 3;


//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE VECTORS
//----------------------------------------------------------------------------------------------------
void iniEmptyArray(int columns, char array[]); // FUNCIONA
void printArray(int columns, char array[]); //FUNCIONA
void putCharOnArray(int columns, char array[], int column, char c); //FUNCIONA
void shiftRightArray(int columns, char array[], int column); //FUNCIONA
void shiftLeftArray(int columns, char array[], int column); //FUNCIONA
void insertCharOnArray(int columns, char array[], int column, char c); //FUNCIONA


//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE MATRIU
//----------------------------------------------------------------------------------------------------
void iniEmptyMatrix(int files, int columns, char matrix[][C_Column]); //FUNCIONA
void printMatrix(int files, int columns, char matrix[][C_Column]); //FUNCIONA
void insertCharOnMatrix(int files, int columns, char matrix[][C_Column], int file, int column, char c); //FUNCIONA
void deleteCharOnMatrix(int files, int columns, char matrix[][C_Column], int file, int column); //FUNCIONA

//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE FINESTRES					     
//----------------------------------------------------------------------------------------------------
void setActiveWindow(int appData[], int aw); //FUNCIONA
void iniWindow(int appData[], char windows[][C_Files][C_Column], int window); //FUNCIONA
void iniWindows(int appData[], char windows[][C_Files][C_Column], int appCursors[][2]); //FUNCIONA
void printWindowsInfo(int appData[], int appCursors[][2]); //FUNCIONA
void printWindow(int appData[], char matrix[C_Files][C_Column]); //FUNCIONA
void printCurrentWindow(int appData[], char windows[][C_Files][C_Column], int appCursors[][2]); //FUNCIONA
bool isLegalFile(int appData[], int y); //FUNCIONA
bool isLegalColumn(int appData[], int x); //FUNCIONA
void setYCursorOnWindow(int appData[], int appCursors[][2], int y); //FUNCIONA
void setXCursorOnWindow(int appData[], int appCursors[][2], int x); //FUNCIONA

//----------------------------------------------------------------------------------------------------
//					EDICIÓ DE TEXT EN FINESTRES 
//----------------------------------------------------------------------------------------------------
void insertCharOnWindow(int appData[], char windows[][C_Files][C_Column], int appCursors[][2], char c); //FUNCIONA
void deleteCurrentPositionOnWindow(int appData[], char windows[][C_Files][C_Column], int appCursors[][2]); //FUNCIONA





//####################################################################################################
//#					PROGRAMA PRINCIPAL					     #
//####################################################################################################

int main()
{
	int appData[4]; // 0, numero finestra; 1, files; 2, columnes; 3, ventana activa	
	appData[0] = C_Windows;
	appData[1]= C_Files;
	appData[2]= C_Column;
	appData[3]= 0;
	char windows[appData[0]] [C_Files][C_Column];
	int appCursors[appData[0]][2] = {0,0};

	

	//
	//
	//CODI DE PROVA
	char matrixprov[5][15];
	iniEmptyMatrix(C_Files, C_Column, matrixprov);
	iniWindow(appData, windows, 0);
	
	
	
	//printWindow(appData, windows[0]);
	//setActiveWindow(appData, 0);

	
	//printCurrentWindow(appData, windows, appCursors); 
	setYCursorOnWindow(appData, appCursors, 3);
	setXCursorOnWindow(appData, appCursors, 4);
	//printf("%d", appData[3]);
	insertCharOnMatrix(C_Files, C_Column, matrixprov, 0, 0, 'B');
	//printMatrix(5,15,matrixprov);
	insertCharOnWindow(appData, windows, appCursors, 'X');
	//printMatrix(appData[1], appData[2], matrixprov);
	printCurrentWindow(appData, windows, appCursors); 
	deleteCurrentPositionOnWindow(appData, windows, appCursors);
	printCurrentWindow(appData, windows, appCursors); 






}

//####################################################################################################
//#					FI PROGRAMA PRINCIPAL					     #
//####################################################################################################


//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE VECTORS
//----------------------------------------------------------------------------------------------------
void iniEmptyArray(int columns, char array[]) //FUNCIONA
{
	for (int i = 0; i <= columns; i++)
	{
		array[i] = {' '}; 
	}
}	

void printArray(int columns, char array[C_Column]) //FUNCIONA
{
	for (int i = 0; i<columns; i++)
	{
		printf("%c", array[i]);
	}
}


void putCharOnArray(int columns, char array[], int column, char c) //FUNCIONA
{
	array[column] = c;
}

void shiftRightArray(int columns, char array[], int column) //FUNCIONA
{
	for (int i = columns; i >= column ; i--)
	{	
		array[i+1] = array[i];
		if (i == column)
		{
			array[column] = ' ';
		}
	}
}

void shiftLeftArray(int columns, char array[], int column) //FUNCIONA
{

	for (int i = column; i < columns; i++)
	{
		array[i] = array[i+1];
	}
	array[columns] = ' ';
}

void insertCharOnArray(int columns, char array[], int column, char c) //FUNCIONA
{
	shiftRightArray(columns, array, column);
	putCharOnArray(columns,  array, column, c);
}

//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE MATRIU
//----------------------------------------------------------------------------------------------------


void iniEmptyMatrix(int files, int columns, char matrix[][C_Column])//FUNCIONA
{
	for (int i = 0; i < files; i++)
	{
		iniEmptyArray(columns, matrix[i]);
	}
}

void printMatrix(int files, int columns, char matrix[C_Files][C_Column]) //FUNCIONA
{ 
	for (int x = 0; x <  files ; x++)
	{
		printf("%2d|", x+1); // +1 potser s'ha de treure
		printArray(columns, matrix[x]);
		printf("|\n");
	}
}

void insertCharOnMatrix(int files, int columns, char matrix[][C_Column], int file, int column, char c)//FUNCIONA
{
	insertCharOnArray(columns, matrix[file], column, c);
}

void deleteCharOnMatrix(int files, int columns, char matrix[][C_Column], int file, int column) //FUNCIONA
{
	shiftLeftArray(columns, matrix[file], column);
	
}
//-----------------------------------------------------------------------------------------------------
//					GESTIÓ DE FINESTRES
//-----------------------------------------------------------------------------------------------------

void setActiveWindow(int appData[], int aw) //FUNCIONA
{
	appData[3] = aw;
}	
void iniWindow(int appData[], char windows[][C_Files][C_Column], int window) //FUNCIONA
{
	iniEmptyMatrix(C_Files, C_Column, windows[window]);
}
void iniWindows(int appData[], char windows[][C_Files][C_Column], int appCursors[][2]) //FUNCIONA
{
	for (int i = 0; i<C_Windows; i++)
	{
		iniWindow(appData, windows, i);
	}
	setActiveWindow(appData, 0); //Al començar el programa comença a la finestra 0
	appCursors[appData[3]][0,0]; //Al començar en una finestra entrem a la posició 0,0
}
void printWindowsInfo(int appData[], int appCursors[][2]) //FUNCIONA
{
 	printf("%d - %d,%d\n", appData[3], appCursors[appData[3]][1], appCursors[appData[3]][2] );
}

void printWindow(int appData[], char matrix[C_Files] [C_Column]) //FUNCIONA
{
	printf("   --------------- \n");
	printMatrix(C_Files, C_Column, matrix);
	printf("   --------------- \n");
}
void printCurrentWindow(int appData[], char windows[][C_Files][C_Column], int appCursors[][2]) //FUNCIONA
{
	printWindowsInfo(appData, appCursors);
	printWindow(appData, windows[appData[3]]);
}
bool isLegalFile(int appData[], int y) //FUNCIONA
{
	return (appData[1] > y); //
}
bool isLegalColumn(int appData[], int x) //FUNCIONA
{
	return (appData[2] > x);
}
void setYCursorOnWindow(int appData[], int appCursors[][2], int y) //FUNCIONA
{
	if (isLegalFile(appData, y))
	{
		appCursors[appData[3]][1] = y;
	}
}
void setXCursorOnWindow(int appData[], int appCursors[][2], int x) //FUNCIONA
{
	if (isLegalFile(appData, x))
	{
		appCursors[appData[3]][2] = x;
	}
}


//----------------------------------------------------------------------------------------------------
//					EDICIÓ DE TEXT EN FINESTRES 
//----------------------------------------------------------------------------------------------------
void insertCharOnWindow(int appData[], char windows[][C_Files][C_Column], int appCursors[][2], char c) //FUNCIONA
{
	insertCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][1], appCursors[appData[3]][2], c);
}
void deleteCurrentPositionOnWindow(int appData[], char windows[][C_Files][C_Column], int appCursors[][2]) //FUNCIONA
{
	deleteCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][1], appCursors[appData[3]][2]);
}


