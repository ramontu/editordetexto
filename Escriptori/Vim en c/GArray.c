#include <stdio.h>

//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE VECTORS
//----------------------------------------------------------------------------------------------------
void iniEmptyArray(int columns, char array[]) //FUNCIONA
{
	for (int i = 0; i <= columns; i++)
	{
		array[i] = ' '; 
	}
}	

void printArray(int columns, char array[]) //FUNCIONA
{
	for (int i = 0; i<columns; i++)
	{
		printf("%c", array[i]);
	}
}




void shiftRightArray(int columns, char array[], int column) //FUNCIONA
{
	for (int i = columns; i >= column ; i--)
	{	
		array[i+1] = array[i];
		if (i == column)
		{
			array[column] = ' ';
		}
	}
	array[columns] = ' ';
}

void shiftLeftArray(int columns, char array[], int column) //FUNCIONA
{

	for (int i = column; i < columns; i++)
	{
		array[i] = array[i+1];
	}
	array[columns] = ' ';
}
void putCharOnArray(int columns, char array[], int column, char c) //FUNCIONA
{
	array[column] = c;	
}
void insertCharOnArray(int columns, char array[], int column, char c) //FUNCIONA
{	
	shiftRightArray(columns, array, column);
	putCharOnArray(columns, array,column,c);	
}

