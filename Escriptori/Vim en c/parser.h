//parser.h
void gotoBeginLine(int appData[], int appCursors[][2]);
void deleteCurrentFile(int appData[], int windows [][appData[1]][appData[2]], int appCursors[appData[3]][2]);
void gotoFirstFile(int appData[], int appCursors[][2]);
void gotoLastFile(int appData[], int appCursors[][2]);
void moveCursorRight(int appData[], int appCursors[][2]);
void moveCursorDown(int appData[], int appCursors[][2]);
void moveCursorUp(int appData[], int appCursors[][2]);
void moveCursorLeft(int appData[], int appCursors[][2]);
void gotoNextWord(int appData[],int windows [][appData[1]][appData[2]], int appCursors[][2]);
void deleteWord(int appData[],int windows [][appData[1]][appData[2]], int appCursors[][2]);
void insertFileDown(int appData[], int appCursors[][2], int windows[][appData[1]][appData[2]]);
void insertFileUp(int appData[], int appCursors[][2], int windows[][appData[1]][appData[2]]);
void parser(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char input);
void parseInput( int appData[], char windows[][appData[1]][appData[2]], int appCursors [][2], char input[]);

