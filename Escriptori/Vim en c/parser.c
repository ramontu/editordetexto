//PARSER TODO PROVAR TOT
#include "GArray.h"
#include "GMatrix.h"
#include "GWindow.h"
#include "stdbool.h"
#include "stdio.h"

void gotoBeginLine(int appData[], int appCursors[][2]) //TODO provar
{
	appCursors[appData[3]][1] = 0; //primera linia
}
void deleteCurrentFile(int appData[], char windows[][appData[1]][appData[2]], int appCursors[appData[3]][2]) //TODO provar
{
	iniEmptyArray(appData[2], windows[appData[3]][appCursors[appData[3]][1]]); //Iniciialitza la linia o la borra
}
void gotoFirstFile(int appData[], int appCursors[][2]) //TODO provar
{
	appCursors[appData[3]][1] = 0;
}

void gotoLastFile(int appData[], int appCursors[][2]) //TODO provar
{
	appCursors[appData[3]][1] = appData[1]-1;
}
void moveCursorRight(int appData[], int appCursors[][2]) //TODO provar
{
	setXCursorOnWindow(appData, appCursors,appCursors[appData[3]][2] +1);
}
void moveCursorDown(int appData[], int appCursors[][2]) //TODO provar
{
	setYCursorOnWindow(appData, appCursors,appCursors[appData[3]][1] +1);

}
void moveCursorUp(int appData[], int appCursors[][2]) //TODO provar
{
	setYCursorOnWindow(appData, appCursors,appCursors[appData[3]][1] -1);
}
void moveCursorLeft(int appData[], int appCursors[][2]) //TODO provar
{
	setXCursorOnWindow(appData, appCursors,appCursors[appData[3]][2] -1);
}
void gotoNextWord(int appData[],char windows [][appData[1]][appData[2]], int appCursors[][2]) //TODO
{

}
void deleteWord(int appData[],char windows [][appData[1]][appData[2]], int appCursors[][2]) //TODO
{
	bool final = false;	
	for (int i = appCursors[appData[3]][2]; (i<appData[2] && !final); i++ )	
	{
		if (windows[appData[3]][appCursors[appData[3]][1]][i] == ' ')
		{
			final = true;
		}
		else
		{
			windows[appData[3]][appCursors[appData[3]][1]][i] = ' ';
		}
	}
}
void insertFileDown(int appData[], int appCursors[][2], char windows[][appData[1]][appData[2]]) //TODO PROVAR
{
	
	for (int fil = appData[1]; fil > appCursors[appData[3]][1]; fil-- )
	{
		for (int col = 0; col < appData[2]; col++)
		{
			windows[appData[3]][fil][col] = windows[appData[3]][fil-1][col];
		}
	}
	setYCursorOnWindow(appData,appCursors, appCursors[appData[3]][1]+1);
	iniEmptyArray(appData[2], windows[appData[3]][appCursors[appData[3]][1]]);
	
	setXCursorOnWindow(appData,appCursors, 0);
}
void insertFileUp(int appData[], int appCursors[][2], char windows[][appData[1]][appData[2]]) //TODO PROVAR
{
	for (int fil = appData[1]; fil > appCursors[appData[3]][1]; fil-- )
	{
		for (int col = 0; col < appData[2]; col++)
		{
			windows[appData[3]][fil][col] = windows[appData[3]][fil-1][col];
			windows[appData[3]][fil-1][col] = ' ';
		}
	}
	setYCursorOnWindow(appData,appCursors, appCursors[appData[3]][1]-1);
	setXCursorOnWindow(appData,appCursors, 0);
	
	
}
void parser(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char input) //TODO PROVAR
{
	static int mode = 0;
	if (mode == 0){
		switch(input)
		{
			case 'i': mode = 1; break; //FUNCIONA
			case 'x': deleteCharOnMatrix(appData[1],appData[2], windows[appData[3]], appCursors[appData[3]][1], appCursors[appData[3]][2]);break;//borra el caracter FUNCIONA
			case 'o': insertFileDown(appData, appCursors, windows);break; // lo mismo que abajo pero se inserta debajo"
			case 'O': insertFileUp(appData, appCursors, windows);break;//inserta nueva linea sobre la actual i coloca el cursor en modo inserción al principio 
			case 'D': deleteCurrentFile(appData, windows, appCursors);break;//borra la línia en que te  encuentras FUNCIONA
			case 'W': deleteWord(appData,windows,  appCursors); break;//borra hasta el final de la palabra
			case 'j': moveCursorDown(appData, appCursors); break; //siguiente linia FUNCIONA
			case 'k': moveCursorUp(appData, appCursors); break; //anterior linia FUNCIONA
			case 'l': moveCursorRight(appData, appCursors); break; //siguiente caracter FUNCIONA
			case 'h': moveCursorLeft(appData, appCursors); break; //anterior caracter FUNCIONA
			case 'g': gotoBeginLine(appData, appCursors); break; //primera linia FUNCIONA
			case 'G': gotoLastFile(appData, appCursors); break; //ultima linea FUNCIONA
			case 'w': 
				while(windows[appData[3]][appCursors[appData[3]][1]][appCursors[appData[3]][2]] == ' ')
				{
					moveCursorRight(appData, appCursors);
				}
								
				while(windows[appData[3]][appCursors[appData[3]][1]][appCursors[appData[3]][2]] != ' ')
				{
					moveCursorRight(appData, appCursors);
				}
				moveCursorRight(appData, appCursors);
				break; 
			case 't':
				  if (appData[3] != (appData[0]-1))
				  {
					  appData[3]++;
				  }
				  else
				  {
					appData[3] = 0;
				  }				
				  break; 
			case 'T':
				  if (appData[3] != 0)
				  {
					  appData[3]--;
				  }
				  else
				  {
					appData[3] = appData[1]-1;
				  }				
				  break; 

		}
	}
	else if (mode == 1) 
	{
		switch(input)
		{
			case '&': mode = 0;break;
			default: 				
				insertCharOnCurrentWindow(appData, appCursors, windows, input);moveCursorRight(appData, appCursors);
				break;
		}
	}
}
	

		
void parseInput( int appData[], char windows[][appData[1]][appData[2]], int appCursors [][2], char input[])
{
	int i = 0;
	while (input[i] != '\0')
	{
		parser(appData, windows,appCursors, input[i]);
		i++;
	}
}

