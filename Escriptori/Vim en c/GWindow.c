#include "GMatrix.h"
#include "stdbool.h" //llibrearia per a que funcionin els boleans
#include "stdio.h"
//-----------------------------------------------------------------------------------------------------
//					GESTIÓ DE FINESTRES
//-----------------------------------------------------------------------------------------------------

void setActiveWindow(int appData[], int aw) //FUNCIONA
{
	appData[3] = aw;
}	
void iniWindow(int appData[], char windows[][appData[1]][appData[2]], int window) //FUNCIONA
{
	iniEmptyMatrix(appData[1], appData[2], windows[window]);
}
void iniWindows(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]) //FUNCIONA
{
	for (int i = 0; i<appData[0]; i++)
	{
		iniWindow(appData, windows, i);
		appCursors[i][1]= 0;
		appCursors[i][2]= 0;
	}
	setActiveWindow(appData, 0); //Al començar el programa comença a la finestra 0
	 //Al començar en una finestra entrem a la posició 0,0
}
void insertCharOnCurrentWindow(int appData[], int appCursors[][2], char windows[][appData[1]][appData[2]], char input)
{
	insertCharOnMatrix(appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][1], appCursors[appData[3]][2], input);
}
void printWindowsInfo(int appData[], int appCursors[][2]) //FUNCIONA
{
 	printf("%d-%d,%d\n", appData[3], appCursors[appData[3]][1], appCursors[appData[3]][2] );
}

void printWindow(int appData[], char matrix[appData[1]][appData[2]]) //FUNCIONA
{
	printf("   --------------- \n");
	printMatrix(appData[1], appData[2], matrix);
	printf("   --------------- \n");
}
void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]) //FUNCIONA
{
	printWindowsInfo(appData, appCursors);
	printWindow(appData, windows[appData[3]]);
}
bool isLegalFile(int appData[], int y) //FUNCIONA
{
	return ((appData[1] > y)&& (y>=0)); //
}
bool isLegalColumn(int appData[], int x) //FUNCIONA
{
	return ((appData[2] > x)&& (x>=0));
}
void setYCursorOnWindow(int appData[], int appCursors[][2], int y) //FUNCIONA
{
	if (isLegalFile(appData, y))
	{
		appCursors[appData[3]][1] = y;
	}
}
void setXCursorOnWindow(int appData[], int appCursors[][2], int x) //FUNCIONA
{
	if (isLegalColumn(appData, x))
	{
		appCursors[appData[3]][2] = x;
	}
}

