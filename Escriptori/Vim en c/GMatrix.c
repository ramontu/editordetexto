#include "GArray.h"
#include "stdio.h"
#include "GWindow.h"
//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE MATRIU
//----------------------------------------------------------------------------------------------------


void iniEmptyMatrix(int files, int columns, char matrix[files][columns])//FUNCIONA
{
	for (int i = 0; i < files; i++)
	{
		iniEmptyArray(columns, matrix[i]);
	}
}

void printMatrix(int files, int columns, char matrix[files][columns]) //FUNCIONA
{ 
	for (int x = 0; x <  files ; x++)
	{
		printf("%2d|", x); // +1 potser s'ha de treure
		printArray(columns, matrix[x]);
		printf("|\n");
	}
}

void insertCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c)//FUNCIONA
{
	insertCharOnArray(columns, matrix[file], column, c);
}

void deleteCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column) //FUNCIONA
{
	shiftLeftArray(columns, matrix[file], column);
	
}

