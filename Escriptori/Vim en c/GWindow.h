//----------------------------------------------------------------------------------------------------
//					GESTIÓ DE FINESTRES					     
//----------------------------------------------------------------------------------------------------
#include "stdbool.h"
void setActiveWindow(int appData[], int aw); //FUNCIONA
void iniWindow(int appData[], char windows[][appData[1]][appData[2]], int window); //FUNCIONA
void iniWindows(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]); //FUNCIONA
void printWindowsInfo(int appData[], int appCursors[][2]); //FUNCIONA
void printWindow(int appData[], char matrix[appData[1]][appData[2]]); //FUNCIONA
void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]); //FUNCIONA
bool isLegalFile(int appData[], int y); //FUNCIONA
bool isLegalColumn(int appData[], int x); //FUNCIONA
void setYCursorOnWindow(int appData[], int appCursors[][2], int y); //FUNCIONA
void setXCursorOnWindow(int appData[], int appCursors[][2], int x); //FUNCIONA
void insertCharOnCurrentWindow(int appData[], int appCursors[][2], char windows[][appData[1]][appData[2]], char input);


